:stylesheet: /home/np/skins/adoc-riak.css

== Datacom toets 
=== periode 2 jan 2023 niveau 4 lj 1

==== Topologie

image::topologie.png[title=Topologie]

==== Opdrachten


===== IP plan
Maak een ip plan voor alle netwerken. Het groene netwerk heeft het subnetmasker *255.255.252.0*. Het rode netwerk *255.255.248.0*. Het groene netwerk is een normaal */24* netwerk.

.Max 20
[%header, cols=2]
|===
|Beoordeling|Max punten
|Geen public ip's gebruikt| 5
|Subnetting correct toegepast|10
|Verschillende netwerken|5
|===

===== First available host adressen op gateway router
Configureer de routers zoveel mogelijk met de first available hostadressen van het netwerk waar de interface bijhoort.

.Max 10
[%header, cols=2]
|===
|Beoordeling|Max punten
|Per juist ingestelde interface|2
|===


===== Last available host op pc groene netwerk
PC-LASTHOST10 in het groene netwerk krijgt het laatst beschikbare adres van het door jou ingestelde netwerk.

.Max 5
[%header, cols=2]
|===
|Beoordeling|Max punten
|Juist ingestelde interface|5
|===

===== Last available host op pc rode netwerk
PC-LASTHOST20 in het rode netwerk krijgt het laatst beschikbare adres van het door jou ingestelde netwerk

.Max 5
[%header, cols=2]
|===
|Beoordeling|Max punten
|Juist ingestelde interface|5
|===

===== Documenteren van netwerkadressen
Plaats een label met het netwerkadres/mask bij alle vier netwerken.

.Max 20
[%header, cols=2]
|===
|Beoordeling|Max punten
|Per correct gelabeld netwerkadres/mask|5
|===

===== Gele DHCP server inrichten
Richt de DHCP server in het gele netwerk in met het juiste subnet en subnetmasker. OF

.Max 5
[%header, cols=2]
|===
|Beoordeling|Max punten
|Service aangezet|1
|Juiste gateway|1
|Juiste ip range|3
|===

===== Bonuspunten Gele DHCP server verwijderen en DHCP op R2 configureren
Je mag de dhcp server ook op R2 inrichten (extra bonus punten). 
*Haal in dat geval de DHCP server in het gele netwerk weg.*

.Max 0 of 10
[%header, cols=2]
|===
|Beoordeling|Max punten
|Naam aan pool gegeven|2
|Juiste range|3
|Adressen of ranges uitsluiten|5
|===
 

===== Adrestoewijzing DHCP server 
De PC's in het gele netwerk krijgen automatisch een adres van de DHCP server

.Max 5 of 15 
[%header, cols=2]
|===
|Beoordeling|Max punten
|PC krijgt adres|2
|PC krijgt gateway|3
|Extra: Router is DHCP server|10
|===

===== Statisch - of dynamisch routeren 
Maak, waar nodig, statische - of dynamische RIP routes aan.

.Max 5 of 10
[%header, cols=2]
|===
|Beoordeling|Max punten
|Per statische route|5
|OSPF juist geconfigureerd|10
|===


==== Beoordeling

Wanneer je de toets zonder bonusopdrachten doet, kun je maximaal een 7.5 halen (75 punten)
Wanneer je de toets met bonusopdrachten doet, kun je maximaal een 10 halen (100 punten)


==== Herkansen
Mocht je een onvoldoende op de toets halen, dan is er in de bufferweek een herkansing gepland.


SUCCES! 


